let MENU = 0;
let s;
let scl = 20;
let score = 0;
let food;

function setup() {
    canvas = createCanvas(800, 800);
    canvas.position(width / 2, 40, 'fixed');

    s = new Snake();
    frameRate(10);
    pickLocation();
}
function draw() {
    strokeWeight(3);
    stroke('black');

    background('#7B8B5F');
    textAlign(CENTER);
    textSize(80);
    textFont('Gloria Hallelujah');
    fill(0, 0, 0);
    text('SNAKE', width / 2, 200);
    textSize(35);
    textFont('Gloria Hallelujah');
    fill(255, 255, 255);
    text('START', width / 2, 400);
    text('SCORE', width / 2, 500);
    text('INSTRUCTIONS', width / 2, 600);

    switch (MENU) {
        case 1:
            background(51);
            s.death();
            if (s.eat(food)) {
                pickLocation();
            }
            s.update();
            s.show();
            fill(255, 0, 100);
            rect(food.x, food.y, scl, scl);
            if (mouseButton == RIGHT) {
                MENU = 0;
            }
            break;
        case 2:
            background('#7B8B5F');

        case 3:
            background('#7B8B5F');
            textSize(20);
            text('Right Click to return to MENU', width / 2, 30);
            textSize(30);
            text(
                '1. Rocks will fall from the top of the screen.',
                width / 2,
                150,
            );
            text('2. Move your snake using arrow keys', width / 2, 200);
            text('<- and -> to avoid being crushed.', width / 2, 240);
            text('3. The game is over when a rock hits you.', width / 2, 290);
            if (mouseButton == RIGHT) {
                MENU = 0;
            }
            break;
    }
}
function pickLocation() {
    const cols = floor(width / scl);
    const rows = floor(height / scl);

    food = createVector(floor(random(cols)), floor(random(rows)));
    food.mult(scl);
}
function keyPressed() {
    if (keyCode === UP_ARROW) {
        s.dir(0, -1);
    } else if (keyCode === DOWN_ARROW) {
        s.dir(0, 1);
    } else if (keyCode === LEFT_ARROW) {
        s.dir(-1, 0);
    } else if (keyCode === RIGHT_ARROW) {
        s.dir(1, 0);
    }
}

function mouseClicked() {
    if (MENU == 0) {
        if (mouseX < width / 2 + 65 && mouseX > width / 2 - 65) {
            if (mouseY < 400 && mouseY > 370) {
                MENU = 1;
            }
            if (mouseY < 500 && mouseY > 470) {
                MENU = 2;
            }
            if (mouseY < 600 && mouseY > 570) {
                MENU = 3;
            }
        }
    }
}
